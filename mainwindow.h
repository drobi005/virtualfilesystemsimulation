#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDirModel>
#include <QInputDialog>
#include <QFileSystemModel>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QString>
#include <QTreeView>
#include <random>
#include <ctime>
#include <QFileDialog>
#include <QStringList>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    //QFile file,fileA,fileB,fileC;
    //const int totalLinesInFile = 58; //The total amount of lines in my text file, listOfPhrases

private slots:
    void    on_pB_AddNewFile_clicked();
    void    on_pB_DeleteAFile_clicked();
    void    on_pB_PrintContents_clicked();
    void    genFilesInA();
    void    genFilesInB();
    void    genFilesInC();
    void    genDriveFolders();
    void    displayFilesInA();
    void    displayFilesInB();
    void    displayFilesInC();
    QString genRandomText();
    QString genRandomName();
private:
    Ui::MainWindow *ui;
    QDirModel *model;
};
#endif // MAINWINDOW_H
