/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTreeView *tV_vNode;
    QPushButton *pB_AddNewFile;
    QPushButton *pB_DeleteAFile;
    QPushButton *pB_PrintContents;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QPlainTextEdit *pTE_FileContents;
    QPlainTextEdit *pTE_iNode;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(866, 579);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        tV_vNode = new QTreeView(centralWidget);
        tV_vNode->setObjectName(QString::fromUtf8("tV_vNode"));
        tV_vNode->setGeometry(QRect(30, 70, 311, 291));
        pB_AddNewFile = new QPushButton(centralWidget);
        pB_AddNewFile->setObjectName(QString::fromUtf8("pB_AddNewFile"));
        pB_AddNewFile->setGeometry(QRect(710, 90, 121, 61));
        pB_DeleteAFile = new QPushButton(centralWidget);
        pB_DeleteAFile->setObjectName(QString::fromUtf8("pB_DeleteAFile"));
        pB_DeleteAFile->setGeometry(QRect(710, 180, 121, 61));
        pB_PrintContents = new QPushButton(centralWidget);
        pB_PrintContents->setObjectName(QString::fromUtf8("pB_PrintContents"));
        pB_PrintContents->setGeometry(QRect(710, 270, 121, 61));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 370, 91, 31));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(130, 40, 101, 31));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(470, 40, 101, 31));
        pTE_FileContents = new QPlainTextEdit(centralWidget);
        pTE_FileContents->setObjectName(QString::fromUtf8("pTE_FileContents"));
        pTE_FileContents->setGeometry(QRect(30, 400, 641, 121));
        pTE_FileContents->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        pTE_iNode = new QPlainTextEdit(centralWidget);
        pTE_iNode->setObjectName(QString::fromUtf8("pTE_iNode"));
        pTE_iNode->setGeometry(QRect(360, 70, 311, 291));
        pTE_iNode->setReadOnly(true);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 866, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Virtual File System Simulation (Project 3)", nullptr));
        pB_AddNewFile->setText(QApplication::translate("MainWindow", "Add New File", nullptr));
        pB_DeleteAFile->setText(QApplication::translate("MainWindow", "Delete A File", nullptr));
        pB_PrintContents->setText(QApplication::translate("MainWindow", "Print File Contents", nullptr));
        label->setText(QApplication::translate("MainWindow", "File Contents:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "v-Node Structure", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "i-Node Structure", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
