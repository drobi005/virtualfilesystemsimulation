#include <iostream>

class iNode
{
public:
	iNode(){}
    iNode(std::string inFileName,std::string inFileExt,std::string inFileCon)
         {fileName=inFileName; fileExt=inFileExt; fileCon=inFileCon;}
	~iNode(){}
    std::string getFileName(){return fileName;}
    std::string getFileExtension(){return fileExt;}
    std::string getFileContents(){return fileCon;}
    void setFileName(std::string a){fileName=a;}
    void setFileExtension(std::string a){fileExt=a;}
    void setFileContents(std::string a){fileCon=a;}

private:
    std::string fileName;
    std::string fileExt;
    std::string fileCon;
};
