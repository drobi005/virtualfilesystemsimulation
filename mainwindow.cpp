#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    model = new QDirModel(this); //create new model
    model->setReadOnly(false);
    model->setSorting(QDir::DirsFirst | QDir::IgnoreCase |QDir::Name); //Sort by directories first, by name
    ui->tV_vNode->setModel(model);
    QModelIndex index = model->index("../VFS/root/"); //relative path to where the VFS.exe is located
    ui->tV_vNode->setModel(model);
    ui->tV_vNode->setRootIndex(model->index("../VFS/root"));
    ui->tV_vNode->expand(index);
    ui->tV_vNode->scrollTo(index);
    ui->tV_vNode->setCurrentIndex(index);
    ui->tV_vNode->resizeColumnToContents(0);

    genDriveFolders(); //Generate Drives A, B, C
    genFilesInA(); //Generate random text files in A
    genFilesInB(); //Generate random text files in B
    genFilesInC(); //Generate random text files in C

    //Update iNode window
    ui->pTE_iNode->appendPlainText("File Name\t  Extension\t     Data");
    displayFilesInA();
    displayFilesInB();
    displayFilesInC();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pB_AddNewFile_clicked()
{
    //Add new file, store the absolute path to the file in fileName
    QString fileName = QFileDialog::getSaveFileName(this, tr("Add File"),
                                                    "../VFS/root/", tr("Text File (*.txt)"));
    qDebug() << "Testing: " << fileName;
    QFile fileT(fileName);
    QTextStream stream(&fileT);
    fileT.open(QIODevice::WriteOnly);
    QString content = QInputDialog::getText(this, "File content", "Enter a line for the file");
    stream << content; //write data to text file
    fileT.close();

    //Update iNode window
    ui->pTE_iNode->clear();
    ui->pTE_iNode->appendPlainText("File Name\t  Extension\t     Data");
    displayFilesInA();
    displayFilesInB();
    displayFilesInC();
    model->refresh();
}

void MainWindow::on_pB_DeleteAFile_clicked()
{
    //Delete a file, store the relative path to the file in fileName
    QString fileName = QFileDialog::getOpenFileName(this, tr("Remove File"),
                                                    "../VFS/root/", tr("Text File (*.txt)"));
    QDir dir;
    dir.remove(fileName);

    //Update iNode window
    ui->pTE_iNode->clear();
    ui->pTE_iNode->appendPlainText("File Name\t  Extension\t     Data");
    displayFilesInA();
    displayFilesInB();
    displayFilesInC();
    model->refresh();
}

void MainWindow::on_pB_PrintContents_clicked()
{
    //Print file contents into pTE_FileContents UI
    QString fileName = QFileDialog::getOpenFileName(this, tr("Pick a File"),
                                                    "../VFS/root/", tr("Text File (*.txt)"));
    QFile fileT(fileName);
    QTextStream stream(&fileT);
    QString line;
    if (fileT.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while(!fileT.atEnd())
        {
            line = stream.readLine();
            //qDebug() << "Name: " <<line;
        }
    }
    fileT.close();
    ui->pTE_FileContents->appendPlainText(line);//setPlainText(line);
}

void MainWindow::genFilesInA()
{
    //Generate a random number of files within drive A. With a unique file name and unique contents.
    int numFiles;
    QString fileName = "../VFS/root/A/";
    //std::default_random_engine seed(time(nullptr));
    std::random_device rd;
    std::mt19937 seed(rd());
    std::uniform_int_distribution<int> genFileNum(2,5);
    numFiles=genFileNum(seed);
    qDebug() << "A: Number of Files: " << numFiles;

    for (int i = 0; i<numFiles; i++)
    {
        //Generate random text file name and file contents
        QString uName = genRandomName()+".txt";
        QString uText = genRandomText();
        //qDebug() << "File Name: " << uName;

        //create file and stream random data into it
        QFile fileT(fileName+uName);
        QTextStream stream(&fileT);
        fileT.open(QIODevice::WriteOnly);;
        stream << uText; //write data to text file
        fileT.close();
        fileName.clear();
        fileName = "../VFS/root/A/"; //reset fileName
        model->refresh();

    }

}
void MainWindow::genFilesInB()
{
    //Generate a random number of files within drive B. With a unique file name and unique contents.
    int numFiles;
    QString fileName = "../VFS/root/B/";
    //std::default_random_engine seed(time(nullptr));
    std::random_device rd;
    std::mt19937 seed(rd());
    std::uniform_int_distribution<int> genFileNum(2,5);
    numFiles=genFileNum(seed);
    qDebug() << "B: Number of Files: " << numFiles;

    for (int i = 0; i<numFiles; i++)
    {
        //Generate random text file name and file contents
        QString uName = genRandomName()+".txt";
        QString uText = genRandomText();
        //qDebug() << "File Name: " << uName;

        //create file and stream random data into it
        QFile fileT(fileName+uName);
        QTextStream stream(&fileT);
        fileT.open(QIODevice::WriteOnly);;
        stream << uText; //write data to text file
        fileT.close();
        fileName.clear();
        fileName = "../VFS/root/B/"; //reset fileName
        model->refresh();
    }
}
void MainWindow::genFilesInC()
{
    //Generate a random number of files within drive C. With a unique file name and unique contents.
    int numFiles;
    QString fileName = "../VFS/root/C/";
    //std::default_random_engine seed(time(nullptr));
    std::random_device rd;
    std::mt19937 seed(rd());
    std::uniform_int_distribution<int> genFileNum(2,5);
    numFiles=genFileNum(seed);
    qDebug() << "B: Number of Files: " << numFiles;

    for (int i = 0; i<numFiles; i++)
    {
        //Generate random text file name and file contents
        QString uName = genRandomName()+".txt";
        QString uText = genRandomText();
        //qDebug() << "File Name: " << uName;

        //create file and stream random data into it
        QFile fileT(fileName+uName);
        QTextStream stream(&fileT);
        fileT.open(QIODevice::WriteOnly);;
        stream << uText; //write data to text file
        fileT.close();
        fileName.clear();
        fileName = "../VFS/root/C/"; //reset fileName
        model->refresh();
    }
}

QString MainWindow::genRandomName()
{
    //Function randomly picks a line of text in the listOfPhrases.txt file
    //std::default_random_engine seed(time(nullptr));
    std::random_device rd;
    std::mt19937 seed(rd());
    QFile fileT("../VFS/listOfNames.txt");
    QTextStream stream(&fileT);
    QString line;
    std::uniform_int_distribution<int> randLine(1,100); //number of lines in file is 100
    int currentLine=randLine(seed); //randomly pick a number that represents a line
    if (fileT.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while(!fileT.atEnd())
        {
            for (int i=0; i<currentLine; i++){line.clear(); line = stream.readLine();} //loop until line is reached
            qDebug() << "Name: " <<line;
        }
    }
    return line;
}

QString MainWindow::genRandomText()
{
    //Function randomly picks a line of text in the listOfPhrases.txt file
    //std::default_random_engine seed(time(nullptr));
    std::random_device rd;
    std::mt19937 seed(rd());
    QFile fileT("../VFS/listOfPhrases.txt");
    QTextStream stream(&fileT);
    QString line;
    std::uniform_int_distribution<int> randLine(1,100); //number of lines in file is 100
    int currentLine=randLine(seed);
    if (fileT.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while(!fileT.atEnd())
        {
            for (int i=0; i<currentLine; i++){line.clear(); line = stream.readLine();}
            qDebug() << "Phrase: " <<line;
        }
    }
    return line;
}

void MainWindow::genDriveFolders()
{
    //Recursively remove all folders on startup
    QDir dir("..\\VFS\\root");
    QDir dirA("..\\VFS\\root\\A");
    QDir dirB("..\\VFS\\root\\B");
    QDir dirC("..\\VFS\\root\\C");
    dirA.removeRecursively();
    dirB.removeRecursively();
    dirC.removeRecursively();
    //Add folders A, B, C
    dir.mkdir("A");
    dir.mkdir("B");
    dir.mkdir("C");
    model->refresh();
}

void MainWindow::displayFilesInA()
{
    QDir A("..\\VFS\\root\\A");
    A.setNameFilters(QStringList()<<"*.txt");
    QStringList fileList = A.entryList(); //store all .txt files into fileList
    QString name;
    //qDebug() <<"Drive A| Index 0: " << fileList[0];
    //qDebug() <<"Files in drive A: " <<fileList.size();
    for (int i=0;i<fileList.size();i++)
    {
        name = fileList[i];
        name.remove(".txt"); //Store only the name of the file
        //qDebug() << "After removal: "<< line;

        //read in a few characters of content in file
        QFile fileT("../VFS/root/A/"+fileList[i]); //add on the current full file name, to the path
        QTextStream stream(&fileT);
        QString contents;
        if (fileT.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            while(!fileT.atEnd())
            {
                contents = stream.readLine(9);
            }
        }
        ui->pTE_iNode->appendPlainText(name+"\t  .txt\t     "+contents+"...");//display file name, extension, and file contents
        fileT.close();
        name.clear();
    }
    fileList.clear();
}

void MainWindow::displayFilesInB()
{
    QDir B("..\\VFS\\root\\B");
    B.setNameFilters(QStringList()<<"*.txt");
    QStringList fileList = B.entryList(); //store all .txt files into fileList
    QString name;
    for (int i=0;i<fileList.size();i++)
    {
        name = fileList[i];
        name.remove(".txt"); //Store only the name of the file
        //qDebug() << "After removal: "<< line;

        //read in a few characters of content in file
        QFile fileT("../VFS/root/B/"+fileList[i]); //add on the current full file name, to the path
        QTextStream stream(&fileT);
        QString contents;
        if (fileT.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            while(!fileT.atEnd())
            {
                contents = stream.readLine(9);
            }
        }
        ui->pTE_iNode->appendPlainText(name+"\t  .txt\t     "+contents+"...");//display file name, extension, and file contents
        fileT.close();
        name.clear();
    }
    fileList.clear();
}

void MainWindow::displayFilesInC()
{
    QDir C("..\\VFS\\root\\C");
    C.setNameFilters(QStringList()<<"*.txt");
    QStringList fileList = C.entryList(); //store all .txt files into fileList
    QString name;
    for (int i=0;i<fileList.size();i++)
    {
        name = fileList[i];
        name.remove(".txt"); //Store only the name of the file
        //qDebug() << "After removal: "<< line;

        //read in a few characters of content in file
        QFile fileT("../VFS/root/C/"+fileList[i]); //add on the current full file name, to the path
        QTextStream stream(&fileT);
        QString contents;
        if (fileT.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            while(!fileT.atEnd())
            {
                contents = stream.readLine(9);
            }
        }
        ui->pTE_iNode->appendPlainText(name+"\t  .txt\t     "+contents+"...");//display file name, extension, and file contents
        fileT.close();
        name.clear();
    }
    fileList.clear();
}
